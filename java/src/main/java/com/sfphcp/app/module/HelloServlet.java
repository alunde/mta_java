package com.sfphcp.app.module;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		Connection conn = null;
		try {
			conn = getConnection();
		} catch (SQLException e) {
			throw new ServletException(e.getMessage(), e);
		}
		try (OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8")) {
			writer.write("Hello World !!");
			writer.write("\n\nJDBC connection available: ");
			if (conn != null) {
				writer.write("yes");
				writer.write("\n\nCurrent Hana DB user:\n");
				String userName = getCurrentUser(conn);
				writer.write(userName);
				writer.write("\n\nCurrent Hana schema:\n");
				writer.write(getCurrentSchema(conn));
				writer.write("\n\nInserting a new temperature record into the sensors.temp table:\n");
				writer.write(getInsertTemp(conn));
				writer.write("\n\nContents of sensors.temp table:\n");
				writer.write(getSensorsTemp(conn));
			} else {
				writer.write("no");
			}
			writer.flush();
			writer.close();
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}

	private String getCurrentUser(Connection conn) throws SQLException {
		String currentUser = "";
		PreparedStatement prepareStatement = conn.prepareStatement("SELECT CURRENT_USER \"current_user\" FROM DUMMY;");
		ResultSet resultSet = prepareStatement.executeQuery();
		int column = resultSet.findColumn("current_user");
		while (resultSet.next()) {
			currentUser += resultSet.getString(column);
		}
		return currentUser;
	}

	private String getCurrentSchema(Connection conn) throws SQLException {
		String currentSchema = "";
		PreparedStatement prepareStatement = conn.prepareStatement("SELECT CURRENT_SCHEMA \"current_schema\" FROM DUMMY;");
		ResultSet resultSet = prepareStatement.executeQuery();
		int column = resultSet.findColumn("current_schema");
		while (resultSet.next()) {
			currentSchema += resultSet.getString(column);
		}
		return currentSchema;
	}

	/**
	 * Returns a psuedo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimim value
	 * @param max Maximim value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max) {
	
	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();
	
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	
	    return randomNum;
	}

	private String getInsertTemp(Connection conn) throws SQLException {
		String newTemp = "";
		int rint = randInt(90,120);
		
		PreparedStatement prepareStatement = conn.prepareStatement("INSERT INTO \"mta_java.db::sensors.temp\" VALUES(\"mta_java.db::tempId\".NEXTVAL," + rint + ",CURRENT_UTCTIMESTAMP,CURRENT_UTCTIMESTAMP)");

		prepareStatement.executeUpdate();
		
		newTemp += "newTemp: " + rint + " ";
		newTemp += "\n";
		
		return newTemp;
	}

	private String getSensorsTemp(Connection conn) throws SQLException {
		String sensorTemps = "";
		PreparedStatement prepareStatement = conn.prepareStatement("SELECT \"tempId\",\"tempVal\",\"created\" FROM \"mta_java.db::sensors.temp\"");
		ResultSet resultSet = prepareStatement.executeQuery();
		int id = resultSet.findColumn("tempId");
		int val = resultSet.findColumn("tempVal");
		int ctd = resultSet.findColumn("created");
		while (resultSet.next()) {
			sensorTemps += "tempId: " + resultSet.getString(id) + " ";
			sensorTemps += "tempVal: " + resultSet.getString(val) + " ";
			sensorTemps += "created: " + resultSet.getDate(ctd) + " ";
			sensorTemps += "\n";
		}
		return sensorTemps;
	}

	private Connection getConnection() throws SQLException {
		try {
			Context ctx = new InitialContext();
			Context xmlContext = (Context) ctx.lookup("java:comp/env");
			DataSource ds = (DataSource) xmlContext.lookup("jdbc/DefaultDB");
			Connection conn = ds.getConnection();
			System.out.println("Connected to database");
			return conn;
		} catch (NamingException ignorred) {
			// could happen if HDB support is not enabled
			return null;
		}
	}
}
