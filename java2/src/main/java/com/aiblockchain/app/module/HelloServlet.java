package com.aiblockchain.app.module;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import java.io.*;
import javax.json.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		Connection conn = null;
		try {
			conn = getConnection();
		} catch (SQLException e) {
			throw new ServletException(e.getMessage(), e);
		}
		try (OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8")) {
			writer.write("Hello World !!");
			writer.write("\n\nJDBC connection available: ");
			if (conn != null) {
				writer.write("yes");
				writer.write("\n\nCurrent Hana DB user:\n");
				String userName = getCurrentUser(conn);
				writer.write(userName);
				writer.write("\n\nCurrent Hana schema:\n");
				writer.write(getCurrentSchema(conn));
				writer.write("\n\nreadFile:\n");
				writer.write(readFile());
			} else {
				writer.write("no");
			}
			writer.flush();
			writer.close();
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}

	private String getCurrentUser(Connection conn) throws SQLException {
		String currentUser = "";
		PreparedStatement prepareStatement = conn.prepareStatement("SELECT CURRENT_USER \"current_user\" FROM DUMMY;");
		ResultSet resultSet = prepareStatement.executeQuery();
		int column = resultSet.findColumn("current_user");
		while (resultSet.next()) {
			currentUser += resultSet.getString(column);
		}
		return currentUser;
	}

	private String getCurrentSchema(Connection conn) throws SQLException {
		String currentSchema = "";
		PreparedStatement prepareStatement = conn.prepareStatement("SELECT CURRENT_SCHEMA \"current_schema\" FROM DUMMY;");
		ResultSet resultSet = prepareStatement.executeQuery();
		int column = resultSet.findColumn("current_schema");
		while (resultSet.next()) {
			currentSchema += resultSet.getString(column);
		}
		return currentSchema;
	}

	private Connection getConnection() throws SQLException {
		try {
			Context ctx = new InitialContext();
			Context xmlContext = (Context) ctx.lookup("java:comp/env");
			DataSource ds = (DataSource) xmlContext.lookup("jdbc/DefaultDB");
			Connection conn = ds.getConnection();
			System.out.println("Connected to database");
			return conn;
		} catch (NamingException ignorred) {
			// could happen if HDB support is not enabled
			return null;
		}
	}
	
	
	// xs bind-service XSA_USER-08k2omm6fuhqk4rc-mta_java-java2 my-fs
	// xs restart XSA_USER-08k2omm6fuhqk4rc-mta_java-java2
	// xs env XSA_USER-08k2omm6fuhqk4rc-mta_java-java2
	
	private String readFile() {
		String output = "";
		String fileName = "test.txt";
        // This will reference one line at a time
        String line = null;
		
		String vcapSVCS = System.getenv("VCAP_SERVICES");		
		
		// output = vcapSVCS;
		
		javax.json.JsonReader jr = javax.json.Json.createReader(new StringReader(vcapSVCS));
		javax.json.JsonObject jo = jr.readObject();

		//Read the fs-storage array.
		javax.json.JsonArray fsstorearray = jo.getJsonArray("fs-storage");
		//Read the first fs-storage item.
		javax.json.JsonObject fsstore = fsstorearray.getJsonObject(0);
		//Read the plan field.
		String plan = fsstore.getString("plan");
		//Read the credentials object.
		javax.json.JsonObject creds = fsstore.getJsonObject("credentials");
		//Read the storage-path field.
		String storepath = creds.getString("storage-path");
		
		output += "StorePath: " + storepath + "\n";

        try {
            // FileReader reads text files in the default encoding.
            
            output += "Opening File: " + fileName + "\n";
            
            FileReader fileReader = 
                new FileReader(storepath + "/" + fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            	output += "Line: " + line + "\n";
            }   

            // Always close files.
           output += "Closing File: " + fileName + "\n";
           bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
        	
        	output += "Unable to Open File: " + fileName + "\n";
        	
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
        	output += "Error Reading File: " + fileName + "\n";
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
        
        
        return output;
	}

}
